IMAGE_NAME = docker.lahaxe.fr:5000/arm/nginx-php7:latest

build :
	docker build -t $(IMAGE_NAME) .

deploy :
	docker push $(IMAGE_NAME)